package main

import (
  "fmt"
)

func main() {
  var n int
  var array = [100]int{}
  fmt.Println("Nhap so phan tu: ")
  fmt.Scan(&n)
  fmt.Println(n)
  for i := 0; i < n; i++ {
    fmt.Print("Nhap phan tu: ")
    fmt.Scan(&array[i])
  }
  newArray := array[0:n]
  fmt.Println(newArray)
  max := findMax(newArray)
  fmt.Println(max)
  min := findMin(newArray)
  fmt.Println(min)
  medianValue := medianNumber(newArray)
  fmt.Println(medianValue)
}

func findMax(array []int) int {
  var max = array[0]
  
  for i:=1; i < len(array); i++ {
    if array[i] > max {
      max = array[i]
    }
  }
  return max
}

func findMin(array []int) int {

  var min = array[0]

  for i := 1; i < len(array); i++ {
    if array[i] < min {
      min = array[i]
    }
  }
  return min 
}

func medianNumber(array []int) float32 {
  var lenArray = len(array)  
  var medianValue float32
  for i:= 0; i < lenArray - 1; i++ {
    for j := i + 1; j < lenArray; j++ {
      if array[i] > array[j] {
        m := array[i]
        array[i] = array[j]
        array[j] = m  
      }
    }
  }
  var middleValue = lenArray/2  
  if lenArray % 2 == 0 {
    medianValue = float32(array[middleValue] + array[middleValue - 1])/2
  } else {
    medianValue = float32(array[middleValue])
  }
  return medianValue
}
