package main

import(
  "fmt"
  "strings"
  "bufio"
  "os"
  "strconv"
)

func main() {
  var input string
  fmt.Print("Nhap chuoi: ")
  scanner := bufio.NewScanner(os.Stdin)
  scanner.Scan()
  input = scanner.Text()
  customInput := strings.Split(input, " ")
  dateOne := strings.Split(customInput[1],"-")
  dateTwo := strings.Split(customInput[2], "-")
  fmt.Println(dateOne, dateTwo)
  dayOne,_ := strconv.ParseInt(dateOne[2], 10, 0)
  monthOne,_ := strconv.ParseInt(dateOne[1], 10, 0)
  yearOne,_ := strconv.ParseInt(dateOne[0], 10, 0)
  fmt.Println(dayOne, monthOne, yearOne)
  dayTwo,_ := strconv.ParseInt(dateTwo[2], 10, 0)
  monthTwo,_ := strconv.ParseInt(dateTwo[1], 10, 0)
  yearTwo,_ := strconv.ParseInt(dateTwo[0], 10, 0)
  fmt.Println(dayTwo, monthTwo, yearTwo)
  valueOne := countDate(dayOne, monthOne, yearOne)
  valueTwo := countDate(dayTwo, monthTwo, yearTwo)
  result := valueOne - valueTwo
  fmt.Println(result)
}


func countDayInMonth(month int64, year int64) int {
  var date int
  switch month  {
      case 2:
         if checkYear(year) {
           date = 29
           break
         } else {
           date = 28
           break
         }
      case 4: case 6: case 9: case 11:
         date = 30
         break
      default:
         date = 31
         break
   }
   return date
}

func countDate(day int64, month int64, year int64) int {
  var count int
  var i int64
  for i = 1; i < year; i++{
    if checkYear(year) {
      count += 366
    } else {
      count += 365
    }
  }
  count = count + countDateFromYear(day, month, year)
  return count
}

func countDateFromYear(day int64, month int64, year int64) int {
  var count int
  var i int64
  for i = 1; i < month; i++{
    count = count +  countDayInMonth(month, year)
  } 
  count = count + int(day)
  return count
}

func checkYear(year int64) bool {
  if  year % 4 == 0 && year % 100 != 0 || year % 400 == 0 {
    return true
  } else {
    return false
  }
}
