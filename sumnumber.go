package main

import(
  "fmt"
	"strconv"
)

func main() {
  var stn1, stn2 string
  fmt.Print("Nhap so thu nhat: ")
  fmt.Scan(&stn1)
  fmt.Print("Nhap so thu hai: ")
  fmt.Scan(&stn2)
  value := sumNumber(stn1, stn2)
  fmt.Println(value)
}

func sumNumber(stn1 string, stn2 string) string {
  var maxIndex int
  total := ""
  if len(stn1) > len(stn2) {
    maxIndex = len(stn1)
  } else {
    maxIndex = len(stn2)
  } 
  var raw int64
  raw = 0
  for i := 0; i < maxIndex; i++ {
    a := getEndNumber(stn1, i+1)
    b := getEndNumber(stn2, i+1)
    rawSum := a + b + raw 
    if rawSum >= 10 {
      raw = getStartNumber(rawSum)
      rawSum = rawSum - 10*raw
    } else {
      raw = 0
    }
    total = strconv.Itoa(int(rawSum)) + total
  }
  if (raw > 0) {
    total = strconv.Itoa(int(raw)) + total
  }
  return total
}

func getEndNumber(number string, index int) int64 {
  var value int64  
  if index > len(number) {
      value = 0
  } else if index == len(number) - 1 && len(number) == 1 {
    numberValue, error := strconv.ParseInt(number, 10, 0)
      if error != nil {
        return 12
      } else {
        value = numberValue
      }
  } else {
      numberValue, error := strconv.ParseInt(string(number[len(number) - index]), 10, 0)
      if error != nil {
        return 12
      } else {
        value = numberValue
      }
  }
  return value
}

func getStartNumber(n int64) int64 {
  var value int64
  for n > 0{
    value = n % 10
    n = n/10
  }
  return value
}
