package main
import ("fmt")

func main() {
  var n int
  fmt.Print("Nhap so n: ")
  fmt.Scan(&n)
  count := countNumber(n)
  fmt.Println(count)
  for n > 0 {
    x := n/exponential(count - 1)
    readNumber(n, x, count)
    n = n - x*exponential(count - 1)
    if n == 0 {
      count = count - 1
      readNumberUnit(n, x, count)
    } else {
      readNumberUnit(n, x, count)
      count = count - 1
    }
  }
}

func countNumber(n int) int {
  count := 0
  for n > 0 {
    n = n/10
    count = count + 1
   }
  return count
}

func exponential(count int) int {
  value := 1
  for i := 0; i < count; i++ {
    value = value*10
  }
  return value
}
func readNumber(n int, x int, count int) {
  if n > 0 && x == 1 && (count == 2 || count == 5) {
    fmt.Print("")
  } else if x == 0 && n >= 1 && n < 10000 && count == 4 {
    fmt.Print("")
  } else if x == 0 && n >= 1 && n < 10000 && count == 5 {
    fmt.Print("")
  } else if x == 0 && n >= 1 && n < 100000 && count == 5 {
    fmt.Print("linh ")
  } else if x == 0 && n >= 1 && n < 100 && count == 2 {
    fmt.Print("linh ")
  } else {
    switch x {
    case 0:
      fmt.Print("khong ")
      break
    case 1:
      fmt.Print("mot ")
      break
    case 2:
      fmt.Print("hai ")
      break
    case 3:
      fmt.Print("ba ")
      break
    case 4:
      fmt.Print("bon ")
      break
    case 5:
      fmt.Print("nam ")
      break
    case 6:
      fmt.Print("sau ")
      break
    case 7:
      fmt.Print("bay ")
      break
    case 8:
      fmt.Print("tam ")
      break
    case 9:
      fmt.Print("chin ")
      break
    default:
      fmt.Print("")
      break
    }
  }
}

func readNumberUnit(n int, x int, count int) {
  if x == 0 && n >= 1 && n < 100000 && count == 5 {
    fmt.Print("")
  } else if x == 0 && n >= 1 && n < 100 && count == 2 {
    fmt.Print("")
  } else if n == 0 {
    if count == 5 {
      fmt.Print("tram nghin ")
    } else if count == 4 {
      fmt.Print("chuc nghin ")
    } else if count == 3 {
      fmt.Print("nghin ")
    } else if count == 2 {
      fmt.Print("tram ")
    } else if count == 1 {
      fmt.Print("muoi ")
    } else {
      fmt.Print("")
    }
  } else {
    switch count {
    case 6:
      fmt.Print("tram ")
      break
    case 5:
      fmt.Print("muoi ")
      break
    case 4:
      fmt.Print("nghin ")
      break
    case 3:
      fmt.Print("tram ")
      break
    case 2:
      fmt.Print("muoi ")
      break
    default:
      fmt.Print("")
      break
    }
  }
}
